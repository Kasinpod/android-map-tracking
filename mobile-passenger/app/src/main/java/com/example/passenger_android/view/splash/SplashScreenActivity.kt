package com.example.passenger_android.view.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.passenger_android.R
import com.example.passenger_android.view.login.LoginActivity
import com.example.passenger_android.view.main.MainActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class SplashScreenActivity : AppCompatActivity() {

    private var handle: Handler? = null
    private var runnable: Runnable? = null
    private var firebaseUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        firebaseUser = FirebaseAuth.getInstance().currentUser
        handle = Handler()

        if (firebaseUser != null) {
            startMain()
        } else {
            startLogin()
        }
    }

    private fun startMain() {
        runnable = Runnable {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun startLogin() {
        runnable = Runnable {
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        handle!!.postDelayed(runnable, 3000)
    }

    override fun onStop() {
        super.onStop()
        handle!!.removeCallbacks(runnable)
    }

}
