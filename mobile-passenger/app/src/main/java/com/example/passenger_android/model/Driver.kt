package com.example.passenger_android.model

data class Driver(
    var lat: Double = 0.0,
    var lng: Double = 0.0,
    var driverId: String = ""
)