package com.example.passenger_android.view.all_driver

import android.os.Bundle
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.passenger_android.R
import com.example.passenger_android.constanst.ConfigDB
import com.example.passenger_android.model.Driver
import com.example.passenger_android.utility.UiHelper
import com.example.passenger_android.utility.collection.MarkerCollection
import com.example.passenger_android.utility.firebase.MapView
import com.example.passenger_android.utility.firebase.FirebaseEventListenerHelper
import com.example.passenger_android.utility.map_helper.GoogleMapHelper
import com.example.passenger_android.utility.map_helper.MarkerAnimationHelper
import com.example.passenger_android.utility.map_helper.callback.LatLngInterpolator
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_driver_all.view.*
import org.koin.android.ext.android.inject

class DriverAllFragment : Fragment(), MapView {

    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var locationFlag = true
    private lateinit var valueEventListener: FirebaseEventListenerHelper
    private val uiHelper: UiHelper by inject()
    private val googleMapHelper: GoogleMapHelper by inject()
    private val markerAnimationHelper: MarkerAnimationHelper by inject()
    private val databaseReference = FirebaseDatabase.getInstance().reference.child(ConfigDB.ONLINE_DRIVERS)
    private lateinit var mView: View
    private val markerCollection = MarkerCollection()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_driver_all, container, false)

        setMap()

        createLocationCallback()

        onInit()

        return mView
    }

    private fun setMap() {
        val mapFragment: SupportMapFragment = childFragmentManager.findFragmentById(R.id.mapAll) as SupportMapFragment
        mapFragment.getMapAsync { googleMap = it }
    }

    private fun onInit() {
        locationProviderClient = LocationServices.getFusedLocationProviderClient(context!!)
        locationRequest = uiHelper.getLocationRequest()

        valueEventListener = FirebaseEventListenerHelper(this)
        databaseReference.addChildEventListener(valueEventListener)

        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if (locationResult!!.lastLocation == null) return
                val latLng = LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                Log.e("Location", latLng.latitude.toString() + " , " + latLng.longitude)
                if (locationFlag) {
                    locationFlag = false
                    val mLatLng = LatLng(
                        locationResult.lastLocation.latitude,
                        locationResult.lastLocation.longitude
                    )
                    googleMap.addMarker(MarkerOptions().position(mLatLng).title("You"))
                    animateCamera(latLng)
                }
            }
        }

    }

    private fun animateCamera(latLng: LatLng) {
        val cameraUpdate = googleMapHelper.buildCameraUpdate(latLng,11f)
        googleMap.animateCamera(cameraUpdate, 10, null)
    }

    override fun onDriverAdded(driver: Driver) {
        val markerOptions = googleMapHelper.getDriverMarkerOptions(LatLng(driver.lat, driver.lng),driver.driverId)
        val marker = googleMap.addMarker(markerOptions)
        marker.tag = driver.driverId
        markerCollection.insertMarker(marker)
        mView.totalOnlineDrivers.text = markerCollection.allMarkers().size.toString()
    }

    override fun onDriverRemoved(driver: Driver) {
        markerCollection.removeMarker(driver.driverId)
        mView.totalOnlineDrivers.text = markerCollection.allMarkers().size.toString()
    }

    override fun onDriverUpdated(driver: Driver) {
        val marker = markerCollection.getMarker(driverId = driver.driverId)
        markerAnimationHelper.animateMarkerToGB(marker!!, LatLng(driver.lat, driver.lng), LatLngInterpolator.Spherical())
    }

    override fun onDestroy() {
        super.onDestroy()
        databaseReference.removeEventListener(valueEventListener)
        locationProviderClient.removeLocationUpdates(locationCallback)
        markerCollection.clearMarkers()
    }

}
