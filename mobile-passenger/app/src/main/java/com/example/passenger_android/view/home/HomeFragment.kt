package com.example.passenger_android.view.home

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.passenger_android.R
import com.example.passenger_android.model.Driver
import com.example.passenger_android.utility.progassBar
import com.example.passenger_android.view.home.adapter.HomeAdapter
import com.example.passenger_android.view.login.LoginActivity
import com.example.passenger_android.view.map.MapsActivity
import com.google.firebase.auth.FirebaseAuth
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment(), HomeView {

    private val presenter = HomePresenter(this)
    private lateinit var mAdapter: HomeAdapter
    private var mView: View ?= null
    private var showProgress: KProgressHUD? = null

    override fun updateUI(`object`: Any) {
        showProgress?.dismiss()
        val data = `object` as List<Driver>
        if (data.isEmpty()) mView?.showError?.visibility = View.VISIBLE
        else mView?.showError?.visibility = View.GONE
        mAdapter.listDriver = data
        mAdapter.notifyDataSetChanged()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_home, container, false)

        getData()

        setRecyclerView(mView!!)

        onEvent(mView!!)

        return mView
    }

    private fun getData() {
        showProgress = progassBar()
        showProgress?.show()
        presenter.getataDriver()
    }

    private fun setRecyclerView(view: View) {

        mAdapter = HomeAdapter {
            val model = it as Driver
            startPageMap(model)
        }

        view.list_driver.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
        }
    }

    private fun onEvent(view: View) {
        view.menu_home.setOnClickListener {
            view.clear_menu.visibility = View.VISIBLE
            view.viewBackground.visibility = View.VISIBLE
            activity!!.viewMain.visibility = View.VISIBLE
        }

        view.clear_menu.setOnClickListener {
            view.clear_menu.visibility = View.GONE
            view.viewBackground.visibility = View.GONE
            activity!!.viewMain.visibility = View.GONE
        }

        view.button_logout.setOnClickListener {
            Toast.makeText(context, "LOGOUT !", Toast.LENGTH_SHORT).show()
            logout()
        }

        view.viewBackground.setOnClickListener {
            view.clear_menu.visibility = View.GONE
            view.viewBackground.visibility = View.GONE
            activity!!.viewMain.visibility = View.GONE
        }
    }

    private fun startPageMap(model: Driver) {
        val intent = Intent(context, MapsActivity::class.java)
        intent.putExtra("driverId",model.driverId)
        startActivity(intent)
    }

    private fun logout() {
        var freebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
        freebaseAuth.signOut()
        freebaseAuth.addAuthStateListener {
            if (freebaseAuth.currentUser == null) {
                val intent = Intent(context, LoginActivity::class.java)
                startActivity(intent)
                activity!!.finish()
            }
        }
    }
}
