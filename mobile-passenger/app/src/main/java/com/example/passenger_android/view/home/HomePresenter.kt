package com.example.passenger_android.view.home

import android.util.Log
import com.example.passenger_android.constanst.ConfigDB
import com.example.passenger_android.model.Driver
import com.google.firebase.database.*

class HomePresenter(private val view: HomeView) {

    private var databaseReference: DatabaseReference? = null

    init {
        databaseReference = FirebaseDatabase.getInstance().reference.child(ConfigDB.ONLINE_DRIVERS)
    }

    fun getataDriver() {
        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("DataLog",p0.message)

            }

            override fun onDataChange(p0: DataSnapshot) {
                var list: ArrayList<Driver> = arrayListOf()
                for (snapshot in p0.children) {
                    val model = snapshot.getValue(Driver::class.java)!!
                    list.add(model)
                }
                view.updateUI(list)
            }
        })
    }
}