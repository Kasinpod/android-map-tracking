package com.example.passenger_android.view.register

import com.example.passenger_android.constanst.ConfigDB
import com.example.passenger_android.model.UserPassenger
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterPresenter(private val view: RegisterView) {

    private var databaseReference: DatabaseReference? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var firebaseUser: FirebaseUser? = null

    init {
        firebaseAuth = FirebaseAuth.getInstance()
    }

    fun register(user: String, email: String, password: String) {
        firebaseAuth?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    firebaseUser = firebaseAuth?.currentUser
                    var userId: String? = null
                    if (firebaseUser != null) userId = firebaseUser?.uid!!

                    databaseReference =
                        FirebaseDatabase.getInstance().getReference(ConfigDB.USER_PASSENGER)
                            .child(userId!!)

                    databaseReference!!.setValue(
                        UserPassenger(
                            userId,
                            email,
                            user
                        )
                    )
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                view.updateUI(it.isSuccessful)
                            }
                        }
                }
            }
    }

}
