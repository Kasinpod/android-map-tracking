package com.example.passenger_android.utility.firebase

import com.example.passenger_android.model.Driver

interface MapView {

    fun onDriverAdded(driver: Driver)

    fun onDriverRemoved(driver: Driver)

    fun onDriverUpdated(driver: Driver)
}