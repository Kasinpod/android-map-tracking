package com.example.passenger_android.di

import com.example.passenger_android.utility.UiHelper
import com.example.passenger_android.utility.map_helper.GoogleMapHelper
import com.example.passenger_android.utility.map_helper.MarkerAnimationHelper
import org.koin.dsl.module

val appModule = module {

    single { GoogleMapHelper() }
    single { MarkerAnimationHelper() }
    single { UiHelper() }

}