package com.example.passenger_android

import android.app.Application
import com.example.passenger_android.di.appModule
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        setupCalligraphy()

        startKoin{
            androidLogger()
            androidContext(this@MyApp)
            modules(listOf(appModule))
        }

    }

    private fun setupCalligraphy() {
        ViewPump.init(
            ViewPump.builder().addInterceptor(
                CalligraphyInterceptor(
                    CalligraphyConfig
                        .Builder()
                        .setDefaultFontPath("fonts/kanit_regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
                )
            ).build()
        )
    }
}