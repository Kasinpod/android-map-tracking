package com.example.passenger_android.view.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.passenger_android.R
import com.example.passenger_android.model.Driver

class HomeAdapter(val callback: (Any) -> Unit) : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    var listDriver: List<Driver> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_online_driver, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = listDriver[position]

        holder.textDriver.text = model.driverId

        holder.textFollow.setOnClickListener {
            callback.invoke(listDriver[position])
        }

        holder.itemView.setOnClickListener {
            callback.invoke(listDriver[position])
        }
    }

    override fun getItemCount(): Int {
        return listDriver.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textDriver: TextView = itemView.findViewById(R.id.textTitleDriver) as TextView
        var textFollow: TextView = itemView.findViewById(R.id.textFollow) as TextView
    }
}