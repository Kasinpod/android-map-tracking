package com.example.passenger_android.view.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.animation.AnimationUtils
import com.example.passenger_android.R
import com.example.passenger_android.utility.progassBar
import com.example.passenger_android.view.main.MainActivity
import com.example.passenger_android.view.register.RegisterActivity
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    private val presenter = LoginPresenter(this)
    private var showProgress: KProgressHUD? = null

    override fun updateUI(`object`: Any) {
        showProgress?.dismiss()
        startMain()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        showProgress = progassBar()

        onEvent()
    }

    private fun onEvent() {
        buttonLogin.setOnClickListener {
            if (validateInput())
                showProgress?.show()
                presenter.setLogin(editTextUserName.text.toString().trim(),editTextPassword.text.toString().trim())
        }

        text_not_have_account.setOnClickListener {
            startRegister()
        }
    }

    private fun validateInput(): Boolean {
        return when {
            editTextUserName.text.toString().trim().isEmpty() -> {
                editTextUserName.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextUserName.requestFocus()
                false
            }
            editTextPassword.text.toString().trim().isEmpty() -> {
                editTextPassword.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextPassword.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun startMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startRegister() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }
}
