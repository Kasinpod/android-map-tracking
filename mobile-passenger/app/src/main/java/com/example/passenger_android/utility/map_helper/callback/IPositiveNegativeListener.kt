package com.example.passenger_android.utility.map_helper.callback

interface IPositiveNegativeListener {

    fun onPositive()

    fun onNegative() {

    }
}