package com.example.passenger_android.model

import com.google.gson.annotations.SerializedName

data class UserPassenger (
    @SerializedName("id")
    val id: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("user_namr")
    val userName: String
)