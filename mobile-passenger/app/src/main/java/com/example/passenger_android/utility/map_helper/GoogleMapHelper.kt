package com.example.passenger_android.utility.map_helper

import android.graphics.Color
import com.example.passenger_android.R
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*

class GoogleMapHelper {

    fun buildCameraUpdate(latLng: LatLng,zoom: Float): CameraUpdate {
        val cameraPosition = CameraPosition.Builder()
            .target(latLng)
            .tilt(TILT_LEVEL.toFloat())
            .zoom(zoom)
            .build()
        return CameraUpdateFactory.newCameraPosition(cameraPosition)
    }

    fun buildCameraMove(latLng: LatLng,zoom: Float): CameraUpdate {
        return CameraUpdateFactory.newLatLngZoom(latLng, zoom)
    }

    fun getDriverMarkerOptions(position: LatLng, title: String): MarkerOptions {
        val options = getMarkerOptions(R.drawable.car_icon, position, title)
        options.flat(true)
        return options
    }

    private fun getMarkerOptions(resource: Int, position: LatLng, title: String): MarkerOptions {
        return MarkerOptions()
            .icon(BitmapDescriptorFactory.fromResource(resource))
            .position(position)
            .title(title)
    }

    fun getPolyline(all: List<LatLng>): PolylineOptions {
        return PolylineOptions()
            .addAll(all)
            .width(20f)
            .startCap(SquareCap())
            .endCap(SquareCap())
            .jointType(JointType.ROUND)
            .color(Color.RED)
            .geodesic(true)
    }

    companion object {
        private const val ZOOM_LEVEL = 18
        private const val TILT_LEVEL = 25
    }
}