package com.example.passenger_android.view.map

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.akexorcist.googledirection.DirectionCallback
import com.akexorcist.googledirection.GoogleDirection
import com.akexorcist.googledirection.constant.*
import com.akexorcist.googledirection.constant.Unit
import com.akexorcist.googledirection.model.Direction
import com.example.passenger_android.R
import com.example.passenger_android.model.Driver
import com.example.passenger_android.utility.UiHelper
import com.example.passenger_android.utility.collection.MarkerCollection
import com.example.passenger_android.utility.firebase.MapView
import com.example.passenger_android.utility.map_helper.GoogleMapHelper
import com.example.passenger_android.utility.map_helper.MarkerAnimationHelper
import com.example.passenger_android.utility.map_helper.callback.LatLngInterpolator
import com.example.passenger_android.utility.progassBarSPIN
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_maps.*
import org.koin.android.ext.android.inject

class MapsActivity : AppCompatActivity(), MapView {

    private val uiHelper: UiHelper by inject()
    private val googleMapHelper: GoogleMapHelper by inject()
    private val markerAnimationHelper: MarkerAnimationHelper by inject()
    private val presenter = MapPresenter(this)
    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var checkFocusUser = true
    private var mLatLng: LatLng? = null
    private lateinit var dest: LatLng
    private var checkFromLatLng = true
    private var checkToLatLng = true
    private var driverId = ""
    private var showProgress: KProgressHUD? = null
    private val markerCollection = MarkerCollection()
    private var fabExpanded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        setMap()

        createLocationCallback()

        onInit()

        onEvent()

    }

    private fun onEvent() {
        buttonBack.setOnClickListener {
            markerCollection.clearMarkers()
            finish()
        }

        feb.setOnClickListener {
            when {
                fabExpanded -> {
                    closeSubMenusFab()
                }
                else -> {
                    openSubMenusFab()
                }
            }
        }

        fabUser.setOnClickListener {
            animateMoveCamera(mLatLng!!, 18f)
            checkFocusUser = false
        }

        fabDriver.setOnClickListener {
            animateMoveCamera(dest, 18f)
            checkFocusUser = true
        }

    }

    private fun setMap() {
        val mapFragment: SupportMapFragment =
            supportFragmentManager.findFragmentById(R.id.supportMap) as SupportMapFragment
        mapFragment.getMapAsync { googleMap = it }
    }

    private fun onInit() {
        driverId = intent.getStringExtra("driverId")
        titleToolbar.text = driverId
        showProgress = progassBarSPIN()
        showProgress?.show()
        locationRequest = uiHelper.getLocationRequest()
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )
    }

    private fun createLocationCallback() {

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if (locationResult!!.lastLocation == null) return
                if (checkFromLatLng) {
                    checkFromLatLng = false
                    mLatLng = LatLng(
                        locationResult.lastLocation.latitude,
                        locationResult.lastLocation.longitude
                    )
                    presenter.getDriverOnline(driverId)
                }
            }
        }
    }

    private fun animateMoveCamera(latLng: LatLng, zoom: Float) {
        val cameraUpdate = googleMapHelper.buildCameraMove(latLng, zoom)
        googleMap.moveCamera(cameraUpdate)
    }

    private fun calculateDirections() {
        val serverKey = "AIzaSyCZmaa6CppOuVX8zSkacrVWVexiErdZrCU"
        GoogleDirection.withServerKey(serverKey)
            .from(dest)
            .to(mLatLng)
            .transportMode(TransportMode.DRIVING)
            .language(Language.THAI)
            .unit(Unit.METRIC)
            .avoid(AvoidType.FERRIES)
            .avoid(AvoidType.INDOOR)
            .execute(object : DirectionCallback {
                override fun onDirectionSuccess(direction: Direction?) {
                    if (direction!!.isOK) {
                        showProgress?.dismiss()
                        titleDetail.visibility = View.VISIBLE
                        card.visibility = View.VISIBLE
                        val route = direction.routeList[0]
                        val leg = route.legList[0]
                        val distanceInfo = leg.distance.text
                        val durationInfo = leg.duration.text
                        val directionPositionList = leg.directionPoint
                        distance.text = "อีก : $distanceInfo จะถึงคุณ"
                        time.text = "ใช้เวลา $durationInfo"
                        googleMap.addPolyline(googleMapHelper.getPolyline(directionPositionList))
                    }
                }

                override fun onDirectionFailure(t: Throwable?) {
                    println("onFailure : ${t?.message}")
                }
            })
    }

    override fun onDriverAdded(driver: Driver) {
    }

    override fun onDriverRemoved(driver: Driver) {
        finish()
        Toast.makeText(this, "$driverId นี้ออฟไลน์ไปแล้ว !", Toast.LENGTH_SHORT).show()
    }

    override fun onDriverUpdated(driver: Driver) {
        println(driver)
        dest = LatLng(driver.lat, driver.lng)
        if (checkToLatLng) {
            checkToLatLng = false
            val markerOptions =
                googleMapHelper.getDriverMarkerOptions(
                    LatLng(driver.lat, driver.lng),
                    driver.driverId
                )
            val marker = googleMap.addMarker(markerOptions)
            animateMoveCamera(dest, 18f)
            marker.tag = driver.driverId
            markerCollection.insertMarker(marker)
            if (mLatLng != null) {
                this.googleMap.addMarker(MarkerOptions().position(mLatLng!!).title("You"))
                calculateDirections()
            }
        } else {
            val marker = markerCollection.getMarker(driverId = driver.driverId)
            marker ?: return
            markerAnimationHelper.animateMarkerToGB(
                marker,
                dest,
                LatLngInterpolator.Spherical()
            )
            if (checkFocusUser) animateMoveCamera(dest, 18f)
        }
    }

    private fun closeSubMenusFab() {
        slideToBottom(fabUser, textViewfabUser, 0)
        slideToBottom(fabDriver, textViewfabDriver, 10)
        fabExpanded = false
    }

    private fun openSubMenusFab() {
        slideToTop(fabUser, textViewfabUser, 0)
        slideToTop(fabDriver, textViewfabDriver, 10)
        fabExpanded = true
    }

    private fun slideToTop(view: View, view2: View, time: Long) {
        val handler = Handler()
        handler.postDelayed(Runnable {
            view.visibility = View.VISIBLE
            view2.visibility = View.VISIBLE
            val animation: Animation = AnimationUtils.loadAnimation(
                this,
                R.anim.slide_to_top
            )
            view.animation = animation
            view2.animation = animation
        }, time)
    }

    private fun slideToBottom(view: View, view2: View, time: Long) {
        val handler = Handler()
        handler.postDelayed(Runnable {
            view.visibility = View.GONE
            view2.visibility = View.GONE
            val animation: Animation = AnimationUtils.loadAnimation(
                this,
                R.anim.slide_to_bottom
            )
            view.animation = animation
            view2.animation = animation
        }, time)
    }

    override fun onDestroy() {
        super.onDestroy()
        markerCollection.clearMarkers()
        locationProviderClient.removeLocationUpdates(locationCallback)
    }
}
