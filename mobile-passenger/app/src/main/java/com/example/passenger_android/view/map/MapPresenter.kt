package com.example.passenger_android.view.map

import android.util.Log
import com.example.passenger_android.constanst.ConfigDB
import com.example.passenger_android.model.Driver
import com.google.firebase.database.*

class MapPresenter(private val view: com.example.passenger_android.utility.firebase.MapView) {

    private var databaseReference: DatabaseReference? = null

    fun getDriverOnline(driverId: String) {
        databaseReference =
            FirebaseDatabase.getInstance().reference.child(ConfigDB.ONLINE_DRIVERS).child(driverId)
//        databaseReference?.addChildEventListener(object :ChildEventListener {
//            override fun onCancelled(p0: DatabaseError) {
//
//            }
//
//            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
//
//            }
//
//            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
//                val driver = p0.getValue(Driver::class.java)
//                view.onDriverUpdated(driver!!)
//            }
//
//            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
//                val driver = p0.getValue(Driver::class.java)
//                view.onDriverAdded(driver!!)
//            }
//
//            override fun onChildRemoved(p0: DataSnapshot) {
//                val driver = p0.getValue(Driver::class.java)
//                view.onDriverRemoved(driver!!)
//            }
//        })

        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("DataLog", p0.message)

            }

            override fun onDataChange(p0: DataSnapshot) {
                val model = p0.getValue(Driver::class.java)
                println("TESTMODEL : $model")
                if (model != null) view.onDriverUpdated(model) else view.onDriverRemoved(Driver())
            }
        })
    }
}