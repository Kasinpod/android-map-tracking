package com.example.passenger_android.remote

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CallApi {

    @GET("maps/api/directions/json")
    fun getDistanceDuration(
        @Query("mode") mode: String,
        @Query("transit_routing_preference") transit_routing_preference: String,
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("key") key: String
    ): Observable<String>
}