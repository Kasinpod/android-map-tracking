package com.example.passenger_android.constanst

object ConfigDB {
    const val ONLINE_DRIVERS = "online_drivers"
    const val USER_PASSENGER = "user_passenger"
}

object ConfigPermission {
    const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 6161
}
