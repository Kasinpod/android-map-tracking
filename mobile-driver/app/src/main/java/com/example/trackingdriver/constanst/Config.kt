package com.example.trackingdriver.constanst

object ConfigDB {
    const val ONLINE_DRIVERS = "online_drivers"
    const val USER_DRIVER = "user_driver"
}

object ConfigPermission {
    const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2200
}

object ConfigMap {
    const val ZOOM_LEVEL = 18
    const val TILT_LEVEL = 25
}
