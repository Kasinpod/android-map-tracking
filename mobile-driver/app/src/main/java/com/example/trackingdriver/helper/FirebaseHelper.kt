package com.example.trackingdriver.helper

import android.util.Log
import com.example.trackingdriver.constanst.ConfigDB
import com.example.trackingdriver.model.Driver
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class FirebaseHelper(driverId: String) {

    private val onlineDriverDatabaseReference: DatabaseReference = FirebaseDatabase
        .getInstance()
        .reference
        .child(ConfigDB.ONLINE_DRIVERS)
        .child(driverId)

    init {
        onlineDriverDatabaseReference
            .onDisconnect()
            .removeValue()
    }

    fun updateDriver(driver: Driver) {
        onlineDriverDatabaseReference
            .setValue(driver)
        Log.e("Driver Info", " Updated")
    }

    fun deleteDriver() {
        onlineDriverDatabaseReference
            .removeValue()
    }
}