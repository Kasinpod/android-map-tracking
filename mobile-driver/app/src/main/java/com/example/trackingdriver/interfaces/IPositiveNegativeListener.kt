package com.example.trackingdriver.interfaces

@FunctionalInterface
interface IPositiveNegativeListener {

    fun onPositive()

    fun onNegative() {

    }
}