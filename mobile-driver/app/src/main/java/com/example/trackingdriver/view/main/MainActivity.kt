package com.example.trackingdriver.view.main

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.trackingdriver.R
import com.example.trackingdriver.constanst.ConfigPermission.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import com.example.trackingdriver.helper.*
import com.example.trackingdriver.interfaces.IPositiveNegativeListener
import com.example.trackingdriver.model.Driver
import com.example.trackingdriver.model.UserDriver
import com.example.trackingdriver.utility.progassBarPIE
import com.example.trackingdriver.view.login.LoginActivity
import com.firebase.jobdispatcher.*
import com.google.android.gms.location.*

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.firebase.auth.FirebaseAuth
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),MainView {

    private val presenter = MainPresenter(this)
    private lateinit var googleMap: GoogleMap
    private lateinit var locationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private var locationFlag = true
    private var driverOnlineFlag = false
    private var currentPositionMarker: Marker? = null
    private val googleMapHelper = GoogleMapHelper()
    private lateinit var firebaseHelper: FirebaseHelper
    private val markerAnimationHelper = MarkerAnimationHelper()
    private val uiHelper = UiHelper()
    private var showProgress: KProgressHUD? = null
    private var driverId = ""

    override fun updateUI(`object`: Any) {
        showProgress?.dismiss()
        val model = `object` as UserDriver
        driverId = model.driverId
        firebaseHelper = FirebaseHelper(model.driverId)
        Log.e("model", model.toString())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showProgress = progassBarPIE()
        showProgress?.show()
        presenter.gatDataUser()

        setMap()

        onEvent()

        onInit()

    }

    private fun setMap() {
        val mapFragment: SupportMapFragment = supportFragmentManager.findFragmentById(R.id.supportMap) as SupportMapFragment
        mapFragment.getMapAsync { p0 -> googleMap = p0!! }
        createLocationCallback()
    }

    private fun onEvent() {
        driverStatusSwitch.setOnCheckedChangeListener { _, b ->
            driverOnlineFlag = b
            if (driverOnlineFlag) {
                driverStatusTextView.text = "ออนไลน์"
                Toast.makeText(this, "ออนไลน์ !", Toast.LENGTH_SHORT).show()
            }
            else {
                driverStatusTextView.text = "ออฟไลน์"
                Toast.makeText(this, "ออฟไลน์ !", Toast.LENGTH_SHORT).show()
                firebaseHelper.deleteDriver()
            }
        }

        menu_main.setOnClickListener {
            clear_menu.visibility = View.VISIBLE
            viewBackground.visibility = View.VISIBLE
        }

        clear_menu.setOnClickListener {
            clear_menu.visibility = View.GONE
            viewBackground.visibility = View.GONE
        }

        button_logout.setOnClickListener {
            Toast.makeText(this, "LOGOUT !", Toast.LENGTH_SHORT).show()
            logout()
        }

        viewBackground.setOnClickListener {
            clear_menu.visibility = View.GONE
            viewBackground.visibility = View.GONE
        }
    }

    private fun onInit() {
        locationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = uiHelper.getLocationRequest()

        if (!uiHelper.isPlayServicesAvailable(this)) {
            Toast.makeText(this, "Play Services did not installed!", Toast.LENGTH_SHORT).show()
            finish()
        } else requestLocationUpdate()
    }

    private fun requestLocationUpdate() {
        if (!uiHelper.isHaveLocationPermission(this)) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
            return
        }
        if (uiHelper.isLocationProviderEnabled(this))
            uiHelper.showPositiveDialogWithListener(this, resources.getString(R.string.need_location), resources.getString(
                R.string.location_content
            ), object :
                IPositiveNegativeListener {
                override fun onPositive() {
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            }, "Turn On", false)
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper())
    }

    private fun createLocationCallback() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                super.onLocationResult(locationResult)
                if (locationResult!!.lastLocation == null) return
                val latLng = LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude)
                Log.e("Location", latLng.latitude.toString() + " , " + latLng.longitude)
                if (locationFlag) {
                    locationFlag = false
                    animateCamera(latLng)
                }
                if (driverOnlineFlag) firebaseHelper.updateDriver(Driver(lat = latLng.latitude, lng = latLng.longitude,driverId = driverId))
                showOrAnimateMarker(latLng)
            }
        }
    }

    private fun showOrAnimateMarker(latLng: LatLng) {
        if (currentPositionMarker == null)
            currentPositionMarker = googleMap.addMarker(googleMapHelper.getDriverMarkerOptions(latLng))
        else {
            RotateMarker.rotateMarker(currentPositionMarker!!, latLng)
//            markerAnimationHelper.animateMarkerToGB(currentPositionMarker!!, latLng, LatLngInterpolator.Spherical())
//            markerAnimationHelper.animateMarkerNew(currentPositionMarker!!, latLng, LatLngInterpolator.Spherical())
//            markerAnimationHelper.animateMarkerNew(latLng,currentPositionMarker)
        }
    }

    private fun animateCamera(latLng: LatLng) {
        val cameraUpdate = googleMapHelper.buildCameraUpdate(latLng)
        googleMap.animateCamera(cameraUpdate, 10, null)
    }

    private fun logout() {
        var freebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
        freebaseAuth.signOut()
        freebaseAuth.addAuthStateListener {
            if (freebaseAuth.currentUser == null) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            val value = grantResults[0]
            if (value == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, "Location Permission denied", Toast.LENGTH_SHORT).show()
                finish()
            } else if (value == PackageManager.PERMISSION_GRANTED) requestLocationUpdate()
        }
    }
}
