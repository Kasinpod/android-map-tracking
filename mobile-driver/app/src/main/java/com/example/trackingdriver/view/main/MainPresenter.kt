package com.example.trackingdriver.view.main

import android.util.Log
import com.example.trackingdriver.constanst.ConfigDB
import com.example.trackingdriver.model.UserDriver
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*

class MainPresenter(private val view: MainView) {

    private var databaseReference: DatabaseReference? = null
    private var firebaseUser: FirebaseUser? = null

    init {
        firebaseUser = FirebaseAuth.getInstance().currentUser
    }

    fun gatDataUser() {
        databaseReference = FirebaseDatabase.getInstance().reference.child(ConfigDB.USER_DRIVER)
            .child(firebaseUser!!.uid)
        databaseReference?.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.e("DataLog", p0.message)

            }

            override fun onDataChange(p0: DataSnapshot) {
                val model = p0.getValue(UserDriver::class.java)!!
                view.updateUI(model)
            }
        })
    }

}