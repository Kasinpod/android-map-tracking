package com.example.trackingdriver.view.register

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.example.trackingdriver.R
import com.example.trackingdriver.utility.progassBar
import com.example.trackingdriver.view.login.LoginActivity
import com.example.trackingdriver.view.main.MainActivity
import com.kaopiz.kprogresshud.KProgressHUD
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(), RegisterView {

    private val presenter = RegisterPresenter(this)
    private var checkShowPassword = false
    private var showProgress: KProgressHUD? = null

    override fun updateUI(`object`: Any) {
        showProgress?.dismiss()
        startMain()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        showProgress = progassBar()

        onEvent()
    }

    private fun onEvent() {
        textShowPassword.setOnClickListener {
            if (checkShowPassword) {
                editTextPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
                editTextConfirmPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance()
            } else {
                editTextPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                editTextConfirmPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
            checkShowPassword = !checkShowPassword
        }

        buttonRegister.setOnClickListener {
            if (checkInput())
                showProgress?.show()
                presenter.register(
                    editTextUserName.text.toString().trim(),
                    editTextEmail.text.toString().trim(),
                    editTextPassword.text.toString().trim(),
                    editTextBusName.text.toString().trim()
                )
        }

    }

    private fun checkInput(): Boolean {
        return when {
            editTextEmail.text.toString().trim().isEmpty() -> {
                editTextEmail.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextEmail.requestFocus()
                false
            }
            editTextBusName.text.toString().trim().isEmpty() -> {
                editTextBusName.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextBusName.requestFocus()
                false
            }
            editTextUserName.text.toString().trim().isEmpty() -> {
                editTextUserName.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextUserName.requestFocus()
                false
            }
            editTextPassword.text.toString().trim().isEmpty() -> {
                editTextPassword.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextPassword.requestFocus()
                false
            }
            editTextConfirmPassword.text.toString().trim().isEmpty() -> {
                editTextConfirmPassword.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextConfirmPassword.requestFocus()
                false
            }
            editTextPassword.text.toString().trim() != editTextConfirmPassword.text.toString().trim() -> {
                editTextPassword.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextPassword.requestFocus()
                false
            }
            else -> {
                true
            }
        }
    }

    private fun startMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        this.finish()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }
}
