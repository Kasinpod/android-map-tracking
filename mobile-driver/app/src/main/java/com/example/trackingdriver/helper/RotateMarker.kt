package com.example.trackingdriver.helper

import android.os.Handler
import android.os.SystemClock
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import com.example.trackingdriver.interfaces.LatLngInterpolator
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin


object RotateMarker {

    private val markerAnimationHelper = MarkerAnimationHelper()

    fun rotateMarker(marker: Marker, latLng2: LatLng) {
        val handler = Handler()
        val toRotation = bearingBetweenLocations(marker.position,latLng2)
        val start = SystemClock.uptimeMillis()
        val startRotation: Float = marker.rotation
        val duration: Long = 1000
        val interpolator: Interpolator = LinearInterpolator()
        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t: Float =
                    interpolator.getInterpolation(elapsed.toFloat() / duration)
                val rot = t * toRotation + (1 - t) * startRotation
                marker.rotation = if (-rot > 180) rot / 2 else rot
                markerAnimationHelper.animateMarkerToGB(marker, latLng2, LatLngInterpolator.Spherical())
                if (t < 1.0) { // Post again 16ms later.
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

    private fun bearingBetweenLocations(latLng1: LatLng, latLng2: LatLng): Float {
        val PI = 3.14159
        val lat1 = latLng1.latitude * PI / 180
        val long1 = latLng1.longitude * PI / 180
        val lat2 = latLng2.latitude * PI / 180
        val long2 = latLng2.longitude * PI / 180
        val dLon = long2 - long1
        val y = sin(dLon) * cos(lat2)
        val x =
            cos(lat1) * sin(lat2) - (sin(lat1)
                    * cos(lat2) * cos(dLon))
        var brng = atan2(y, x)
        brng = Math.toDegrees(brng)
        brng = (brng + 360) % 360
        return brng.toFloat()
    }
}