package com.example.trackingdriver.model

data class UserDriver (
    val id: String = "",
    val email: String = "",
    val userName: String = "",
    val driverId: String = ""
)