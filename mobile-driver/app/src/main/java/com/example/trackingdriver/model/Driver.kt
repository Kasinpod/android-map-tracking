package com.example.trackingdriver.model

data class Driver(
    val lat: Double,
    val lng: Double,
    val driverId: String
)