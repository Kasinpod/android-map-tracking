package com.example.trackingdriver.view.login

import com.google.firebase.auth.FirebaseAuth

class LoginPresenter(private val view: LoginView) {
    private var firebaseAuth: FirebaseAuth? = null

    init {
        firebaseAuth = FirebaseAuth.getInstance()
    }

    fun setLogin(email: String, password: String) {
        firebaseAuth!!.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    view.updateUI(it.isSuccessful)
                } else {

                }
            }
    }
}