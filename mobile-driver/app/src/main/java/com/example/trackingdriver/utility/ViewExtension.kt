package com.example.trackingdriver.utility

import android.app.Activity
import androidx.fragment.app.Fragment
import com.kaopiz.kprogresshud.KProgressHUD

fun Fragment.progassBar() : KProgressHUD {
    return KProgressHUD.create(context)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        .setDimAmount(0.5f)
}

fun Activity.progassBar() : KProgressHUD {
    return KProgressHUD.create(this)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
}

fun Activity.progassBarPIE() : KProgressHUD {
    return KProgressHUD.create(this)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        .setLabel("โปรดรอสักครู่!")
}
