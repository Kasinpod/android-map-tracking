package com.example.trackingdriver.view.register

import com.example.trackingdriver.constanst.ConfigDB
import com.example.trackingdriver.model.UserDriver
import com.example.trackingdriver.view.register.RegisterView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class RegisterPresenter(private val view: RegisterView) {

    private var databaseReference: DatabaseReference? = null
    private var firebaseAuth: FirebaseAuth? = null
    private var firebaseUser: FirebaseUser? = null

    init {
        firebaseAuth = FirebaseAuth.getInstance()
    }

    fun register(user: String, email: String, password: String, busName: String) {
        firebaseAuth?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    firebaseUser = firebaseAuth?.currentUser
                    var userId: String? = null
                    if (firebaseUser != null) userId = firebaseUser?.uid!!

                    databaseReference =
                        FirebaseDatabase.getInstance().getReference(ConfigDB.USER_DRIVER)
                            .child(userId!!)

                    databaseReference!!.setValue(
                        UserDriver(
                            userId,
                            email,
                            user,
                            busName
                        )
                    )
                        .addOnCompleteListener {
                            if (it.isSuccessful) {
                                view.updateUI(it.isSuccessful)
                            }
                        }
                }
            }
    }

}
